# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from queue_runner_v2.handlers import api_request


class RequestsMock:
    """request class to mock the requests lib and mock api calls."""

    def __init__(
        self, request_timeout=None, response_mock=None, requests_exception=None
    ):
        if response_mock is None:
            self.response_mock = ResponseMock(
                text="request successful", status_code=200, headers="request"
            )
        else:
            self.response_mock = response_mock

        if requests_exception is None:
            self.requests_exception = False
        else:
            self.requests_exception = requests_exception

        self._request_timeout = request_timeout

    def post(self, url: str, json, verify: bool, timeout):
        if self.requests_exception:
            raise self.requests_exception()
        else:
            return self.response_mock


class ResponseMock:
    __slots__ = [
        "text",
        "status_code",
        "headers",
        "json_response",
        "reason",
        "url",
    ]

    def __init__(self, text, status_code, headers, json_response=None, url=""):
        self.reason = "reason why request failed"
        self.text = text
        self.status_code = status_code
        self.headers = {"zs-req-id": headers}
        self.url = url
        if json_response is None:
            self.json_response = None
        else:
            self.json_response = json_response

    def json(self):
        if self.json_response:
            return self.json_response
        else:
            raise ValueError


class TestApiHandler:
    def test_init_values(self):
        cert = "/tls_certifate.crt"
        handler = api_request.HandlerApi(
            request_module=RequestsMock(),
            request_timeout=437,
            certificate=cert,
        )
        assert handler._certificate is cert
        assert handler._timeout == 437

    def test_empty_url(self):
        """Check no url in message raises a KeyError."""
        msg = {"body": "body msg"}
        try:
            api_request.HandlerApi(request_timeout=60).execute(
                msg, routing_key="test"
            )
            assert False
        except KeyError:
            assert True

    def test_successful_request(self, caplog):
        """Test successful Api call from beginning till end."""
        caplog.set_level(logging.INFO)
        handler = api_request.HandlerApi(
            request_module=RequestsMock(
                response_mock=ResponseMock(
                    text="request successful",
                    status_code=200,
                    headers="request",
                    json_response={
                        "result": [
                            {"type": "msg_type", "messages": ["msg1", "msg2"]}
                        ]
                    },
                )
            ),
            request_timeout=600,
        )
        msg = {"url": "extra_special_url", "msg": "msg as expected"}
        try:
            handler.execute(msg, routing_key="test")
            assert "extra_special_url" in caplog.records[0].message
            assert (
                "Queue item handled successfully." in caplog.records[1].message
            )
        except Exception:
            assert False

    def test_successful_request_corrupt_item(self, caplog):
        caplog.set_level(logging.INFO)
        handler = api_request.HandlerApi(
            request_module=RequestsMock(
                response_mock=ResponseMock(
                    text="request successful",
                    status_code=200,
                    headers="request",
                )
            ),
            request_timeout=600,
        )
        msg = {"url": "extra_special_url", "msg": "msg as expected"}
        try:
            handler.execute(msg, routing_key="test")
            assert "extra_special_url" in caplog.records[0].message
            assert "Corrupt response body" in caplog.records[2].message
        except Exception:
            assert False

    def test_failed_request(self, caplog):
        handler = api_request.HandlerApi(
            request_timeout=600,
            request_module=RequestsMock(requests_exception=Exception),
        )
        msg = {"url": "localhost", "msg": "msg as expected"}
        try:
            handler.execute(msg, routing_key="test")
            assert False
        except Exception:
            assert "Error during http(s) request" in caplog.records[0].message

    def test_http_error_code_failed_parsing_json(self, caplog):
        json_response = None
        response = ResponseMock(
            text="TEXTLOGGING TEST",
            status_code=400,
            headers="request_id",
            json_response=json_response,
        )
        handler = api_request.HandlerApi(
            request_timeout=600,
            request_module=RequestsMock(response_mock=response),
        )
        msg = {"url": "localhost", "msg": "msg as expected"}

        try:
            handler.execute(msg, routing_key="test")
            assert False
        except Exception:
            assert response.text in caplog.records[0].message

    def test_http_error_code_with_good_json(self, caplog):
        from requests.exceptions import HTTPError

        json_response = {
            "result": [{"type": "msg_type", "messages": ["msg1", "msg2"]}]
        }
        headers = "request_id"
        response = ResponseMock(
            text="TEXTLOGGING TEST",
            status_code=400,
            headers=headers,
            json_response=json_response,
        )
        handler = api_request.HandlerApi(
            request_timeout=600,
            request_module=RequestsMock(response_mock=response),
        )
        msg = {"url": "test.localhost", "msg": "msg as expected"}
        try:
            handler.execute(msg, routing_key="test")
            assert False
        except HTTPError:
            status_code = "400"

            assert status_code in caplog.records[0].message
            for msg in json_response["result"][0]["messages"]:
                assert msg in caplog.records[0].message

    def test_request_timeout(self, caplog):
        import requests

        handler = api_request.HandlerApi(
            request_timeout=60,
            request_module=RequestsMock(
                requests_exception=requests.exceptions.ConnectTimeout
            ),
        )
        msg = {"url": "test.localhost", "msg": "msg as expected"}
        try:
            handler.execute(msg, routing_key="test")
            assert False
        except Exception:
            assert (
                "HTTP timeout after exceeding the time limit:"
                in caplog.records[0].message
            )

    def test_successful_response_wrong_json_format(self, caplog):
        """Test successful Api call but returning json in the wrong format."""
        caplog.set_level(logging.INFO)
        # init handler
        handler = api_request.HandlerApi(
            request_module=RequestsMock(), request_timeout=600
        )
        # response
        response_mock = ResponseMock(
            text="request successful",
            status_code=200,
            headers="request",
            json_response={
                "no_results": [
                    {"type": "msg_type", "messages": ["msg1", "msg2"]}
                ]
            },
            url="extra_special_url",
        )
        handler._successful_response(
            response=response_mock,
            request_id="request_id",
            routing_key="routing.key",
        )
        assert "Queue item handled successfully." in caplog.records[0].message
        assert "Corrupt response body:" in caplog.records[1].message

    def test_unsuccessful_response_wrong_json_format(self, caplog):
        """Test successful Api call but returning json in the wrong format."""
        caplog.set_level(logging.INFO)
        # init handler
        handler = api_request.HandlerApi(
            request_module=RequestsMock(), request_timeout=600
        )
        # response
        response_mock = ResponseMock(
            text="showsup_in_log?",
            status_code=400,
            headers="request",
            json_response={
                "no_results": [
                    {"type": "msg_type", "messages": ["msg1", "msg2"]}
                ]
            },
            url="extra_special_url",
        )
        handler._unsuccessful_response(
            response=response_mock, request_id="request_id"
        )
        assert "Error while running action" in caplog.records[0].message
        assert "showsup_in_log?" in caplog.records[0].message
