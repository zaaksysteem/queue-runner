# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


def test_mockstatsd(caplog):
    from queue_runner_v2.__init__ import StatsDMock
    import logging

    caplog.set_level(logging.INFO)

    statsd = StatsDMock()

    statsd.incr("incr")
    statsd.timer("timer")
    assert "incr" in caplog.records[0].message
    assert "timer" in caplog.records[1].message
